import flask
import models
from pathlib import Path
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


def init_app(app):
    """Inititialize the database

    Creates an engine, ensure all tables are created and set a session.
    """
    # create db in current path
    path = Path(__file__).parent / 'webclient.db'

    # create the engine
    engine = create_engine(
        f"sqlite:///{path}", connect_args={'check_same_thread': False})

    # bind metadata to engine
    if not models.metadata.is_bound():
        models.metadata.bind = engine
        models.metadata.create_all()

    # initialize the session
    models.session = scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine))
