import json
import blog
import auth
import data
import flask

# create an app
app = flask.Flask(__name__)
# set a secret, required for sessions
app.secret_key = '_5#y2L"F4Q8z\n\xec]/'
# API endpoint
app.config['API'] = 'http://127.0.0.1:8081'

# add module blueprints
app.register_blueprint(blog.bp)
app.register_blueprint(auth.bp)
# initialize the database
data.init_app(data)

@app.route('/')
def index():
    """Landing page
    """
    return flask.render_template('index.html')

if __name__ == '__main__':
    app.run(port=8082, debug=True)
