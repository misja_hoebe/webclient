import flask
import logging
import requests
from auth import login_required

logger = logging.getLogger(__name__)
bp = flask.Blueprint('blog', __name__, url_prefix='/blog')


def api_endpoint(resource=None, collection='posts'):
    """Create an endpoint url
    """
    endpoint = [flask.current_app.config['API'], collection]
    if resource:
        endpoint.append(resource)
    return '/'.join(endpoint)


@bp.route('/')
def get_posts():
    """Get posts
    """
    posts = requests.get(api_endpoint()).json()
    return flask.render_template('blog/index.html', posts=posts)


@bp.route('/create', methods=['GET', 'POST'])
@login_required
def create_post():
    """Create a post
    """
    if flask.request.method == 'POST':
        data = {
            'author': flask.g.user.username,
            'title': flask.request.form['title'],
            'content': flask.request.form['content']
        }

        try:
            response = requests.post(api_endpoint(), json=data)
            response.raise_for_status()
        except requests.HTTPError as exc:
            flask.flash(str(exc), 'error')

        return flask.redirect(flask.url_for('blog.get_posts'))

    return flask.render_template('blog/create.html')


@bp.route('/update/<string:post_id>', methods=('GET', 'POST'))
@login_required
def update_post(post_id):
    """Update a post
    """
    if flask.request.method == 'POST':
        post = {
            'author': flask.g.user.username,
            'title': flask.request.form['title'],
            'content': flask.request.form['content']
        }

        try:
            response = requests.put(api_endpoint(post_id), json=post)
            response.raise_for_status()
        except requests.HTTPError as exc:
            flask.flash(str(exc), 'error')
    else:
        try:
            response = requests.get(api_endpoint(post_id))
            response.raise_for_status()
            post = response.json()
        except requests.HTTPError as exc:
            flask.flash(str(exc), 'error')
            return flask.redirect(flask.url_for('blog.get_posts'))

    return flask.render_template('blog/update.html', post=post)


@bp.route('/delete/<string:post_id>', methods=('POST',))
@login_required
def delete_post(post_id):
    """Delete a post
    """
    try:
        response = requests.delete(api_endpoint(post_id))
        response.raise_for_status()
        flask.flash(f"Post {post_id} deleted")
    except requests.HTTPError as exc:
        flask.flash(str(exc), 'error')
    return flask.redirect(flask.url_for('blog.get_posts'))
