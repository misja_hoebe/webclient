import flask
import models
import logging
import functools
from models import User
from flask import request
from werkzeug.security import check_password_hash, generate_password_hash

logger = logging.getLogger(__name__)
bp = flask.Blueprint('auth', __name__, url_prefix='/auth')


def get_user(username):
    """Get a user
    """
    return models.session \
        .query(User) \
        .filter(User.username == username) \
        .first()


def login_required(view):
    """Logged in view decorator

    Checks if the user has a valid session.
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if flask.g.user is None:
            return flask.redirect(flask.url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view


@bp.before_app_request
def set_session_user():
    """Set current user for this request
    """
    user_id = flask.session.get('user_id')
    if user_id:
        flask.g.user = models.session.query(User).get(user_id)
    else:
        flask.g.user = None


@bp.route('/login', methods=('GET', 'POST'))
def login():
    """Login a user
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if not all([username, password]):
            flask.flash('Missing name or password')
        else:
            user = get_user(username)
            if not user:
                flask.flash('Invalid credentials')
            else:
                if not check_password_hash(user.password, password):
                    flask.flash('Invalid login')
                else:
                    # create a new session
                    flask.session.clear()
                    flask.session['user_id'] = user.id
                    # redirect to home
                    return flask.redirect(flask.url_for('index'))

    return flask.render_template('auth/login.html')


@bp.route('/logout')
def logout():
    """Logout a user
    """
    flask.session.clear()
    return flask.redirect(flask.url_for('index'))


@bp.route('/register', methods=('GET', 'POST'))
def register():
    """Register a user
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if not all([username, password]):
            flask.flash('Missing name or password')
        else:
            user = get_user(username)
            if user:
                flask.flash('User already registered')
            else:
                password = generate_password_hash(password)
                user = User(username=username, password=password)
                models.session.add(user)
                models.session.commit()
                flask.flash(f'User {username} created')

    return flask.render_template('auth/register.html')
