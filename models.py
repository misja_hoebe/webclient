import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, Column, String, Integer

session = None
metadata = MetaData()
Base = declarative_base(metadata=metadata)


class User(Base):
    """User model
    """
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
